# Quake Reports System

TrustVox project of Quake III reports system from the BackEnd Developer Test

# Description

This project was made for the BackEnd Developer test of Trustvox. Was developed in ruby with use of Rspec for UnitTest and BDD and make use of OptionParser gem to generate a command line app.

Task 1
[Reports System Explained]( https://gitlab.com/dkretzlaff/quake_reports/blob/5a71a2a1377e1591807c7db7909cd62d2c7e3bc5/Reports_System_Explained.txt)


Task 2
# How to run
> $ cd path/to/project/quake_reports/ <br>
> $ bundle install <br>
> $ ruby bin/reports_system.rb -f <LOG FILE> [options]

# Examples

Listing games
> $ ruby bin/reports_system.rb -f log/quake.txt -g <br>
> ["game0", "game1", "game2", "game3", "game4", ...] 

Summary of game
> $ ruby bin/reports_system.rb -f log/quake.txt -g game_1 <br>
> Summary of game: <br>
> 	1 - Player names <br>
>	2 - Total number of kills <br>
>	3 - Total kills by players


Listing players names of game
> $ ruby bin/reports_system.rb -f log/quake.txt -g game_0 -o 1 <br>
> ["Isgalamido"]

Total number of kills in a game
> $ ruby bin/reports_system.rb -f log/quake.txt -g game_0 -o 2 <br>
> 0

Total kills by players
> $ ruby bin/reports_system.rb -f log/quake.txt -g game_0 -o 3 <br>
> [{"Isgalamido"=>0}]

Export to json file
> $ ruby bin/reports_system.rb -f log/quake.txt -e text.json <br>
> Json file generated text.json

Help Page
> $ ruby bin/reports_system.rb -h <br>
>Usage: reports_system.rb -f <FILE> [options] <br>
>    -g, --game GAME>                 Show game summary <br> 
>    -o, --option OPTION              Show option selected in sumary of game <br>
>    -s, --summary                    Show game summary <br>
>    -a, --all_games                  List all games in log <br>
>    -f, --file FILE                  Read a file log <br> 
>    -j, --export_to_json FILE        Export to json file <br>
>    -h, --help                       Displays Help
